<?php
date_default_timezone_set('Europe/Berlin');

$servername = "localhost";
$username = "DBuser";
$password = "DBpassword";
$dbname = "starlink";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

$day=$_GET["day"];
$hour=$_GET["hour"];



$sql = "SELECT id, datetime, weather, ping, down, up, status FROM internet_speed";
$day_array=array();
$db_array=array();


$result = $conn->query($sql);

if ($result->num_rows > 0) {
  // output data of each row
  while($row = $result->fetch_assoc()) {
	$date_only=date("Y-m-d", $row["datetime"]);
	if (!in_array($date_only, $day_array)) {
		array_push($day_array, $date_only);
	}
	
	array_push($db_array, array($row["id"],$row["datetime"],$row["ping"],$row["down"],$row["up"],$row["status"]));
	
  }
} else {
  echo "0 results";
}
$conn->close();

//var_dump($day_array);

if($day != ""){
	echo "<a href='?' target='_self'>Return</a><br>";
}

$day_min = strtotime($day);
$day_max = strtotime("+1 day", $day_min);

$hour_min = strtotime($day." ".$hour.":00");
$hour_max = strtotime("+1 hours", $hour_min);

$hour_array=array();

$final_array=array();

$array_for_each=array();

foreach ($db_array as &$db_value) {
	$array_id=$db_value[0];
	$array_timestamp=$db_value[1];
	$array_ping=$db_value[2];
	$array_download=$db_value[3];
	$array_upload=$db_value[4];
	$array_status=$db_value[5];
	
	if($day != ""){
		if($array_timestamp > $day_min & $array_timestamp < $day_max){
			$add_data=1;
			$hout_only=date("H", $array_timestamp);
			if (!in_array($hout_only, $hour_array)) {
				array_push($hour_array, $hout_only);
			}
		}else{
			$add_data=0;
		}
		
		if($hour != ""){
			if($array_timestamp > $hour_min & $array_timestamp < $hour_max){
				$add_data=1;
			}else{
				$add_data=0;
			}
		}
	}else{
		$date_only=date("Y-m-d", $array_timestamp);
		if ($array_for_each[$date_only]) {
			$array_for_each[$date_only][0]=$array_for_each[$date_only][0] + $array_download;
			$array_for_each[$date_only][1]=$array_for_each[$date_only][1] + $array_upload;
			$array_for_each[$date_only][2]=$array_for_each[$date_only][2] + $array_ping;
			$array_for_each[$date_only][3]=$array_for_each[$date_only][3] + 1;
		}else{
			$array_for_each[$date_only] = array($array_download,$array_upload,$array_ping,1,$array_timestamp,$date_only);
		}
	}
	if($add_data==1){
		array_push($final_array, $db_value);
	}
}

$data_download = "";
$data_upload = "";
$data_ping = "";
$data_category = "";
$cound_all=0;


if($day == ""){
	foreach ($array_for_each as &$array_for_each_value) {
		$cound_all++;
		$array_download_row=$array_for_each_value[0];
		$array_upload_row=$array_for_each_value[1];
		$array_ping_row=$array_for_each_value[2];
		$array_max=$array_for_each_value[3];
		$array_timestamp=$array_for_each_value[4];
		
		$array_download=round(($array_download_row / $array_max), 2);
		$array_upload=round(($array_upload_row / $array_max), 2);
		$array_ping=round(($array_ping_row / $array_max), 2);
		
		if($data_download == ""){
				 $data_download=$array_download;
			 }else{
				 $data_download.=",".$array_download;
			}
			if($data_upload == ""){
				$data_upload=$array_upload;
			}else{
				$data_upload.=",".$array_upload;
			}
			
			if($data_ping == ""){
				 $data_ping=$array_ping;
			}else{
				 $data_ping.=",".$array_ping;
			}
			$date_only=date("Y-m-d", $array_timestamp);
			$date_time=date("Y_m_d_H_i_s", $array_timestamp);
			
			if($data_category == ""){
				 
				 $data_category="'".$date_only."'";
			}else{
				 $data_category.=",'".$date_only."'";
			}
	}
}
else{
	foreach ($final_array as &$final_value) {
		$cound_all++;
		$array_id=$final_value[0];
		$array_timestamp=$final_value[1];
		$array_ping=$final_value[2];
		$array_download=$final_value[3];
		$array_upload=$final_value[4];
		$array_status=$final_value[5];
		
			if($data_download == ""){
				 $data_download=$array_download;
			 }else{
				 $data_download.=",".$array_download;
			}
			if($data_upload == ""){
				$data_upload=$array_upload;
			}else{
				$data_upload.=",".$array_upload;
			}
			
			if($data_ping == ""){
				 $data_ping=$array_ping;
			}else{
				 $data_ping.=",".$array_ping;
			}
			$date_only=date("Y-m-d", $array_timestamp);
			$date_time=date("Y_m_d_H_i_s", $array_timestamp);
			
			if($data_category == ""){
				 
				 $data_category="'".$date_time."'";
			}else{
				 $data_category.=",'".$date_time."'";
			}
	}
}

if($day != ""){
	echo "<br><br> Selected: ".$day. "(".$cound_all.")";
	echo "<br>Hours: ";
	//var_dump($hour_array);
	foreach ($hour_array as &$value) {
		echo "<a href='?day=".$day."&hour=".$value."' target='_self'>".$value."</a>, ";
	}
}else{
	$day_count=0;
	foreach ($day_array as &$value) {
		$day_count++;
		
		foreach ($array_for_each as &$array_for_each_value) {
			if($array_for_each_value[5] == $value){
				$array_download_row=$array_for_each_value[0];
				$array_upload_row=$array_for_each_value[1];
				$array_ping_row=$array_for_each_value[2];
				$array_max=$array_for_each_value[3];
				
				$array_download=round(($array_download_row / $array_max), 2);
				$array_upload=round(($array_upload_row / $array_max), 2);
				$array_ping=round(($array_ping_row / $array_max), 2);
			}
		}
		
		
		echo $day_count.". Day: <a href='?day=".$value."' target='_self'>".$value."</a> (AD:".$array_download.",AU:".$array_upload.",AP:".$array_ping.", )<br>";
	}
	echo "<br>AD = average download, AU = average upload, AP = average ping<br>";
	echo "<br>the average data per day, click on date and hours to see more data"." (".$cound_all.")";;
}
//echo $data_download."<br>";
//echo $data_upload."<br>";
//echo $data_ping."<br>";
//echo "cat:".$data_category."<br>";

//$timestamp = time();
//$datum = date("d.m.Y - H:i", $timestamp);
//echo $datum;

?>
<html>
  <head>
    <!-- Load c3.css -->
<link href="c3.css" rel="stylesheet">
  </head>
  <body>
  
<div id="chart"></div>


<!-- Load d3.js and c3.js -->
<script src="d3-5.8.2.min.js" charset="utf-8"></script>
<script src="jquery-3.3.1.min.js"></script>
<script src="c3.min.js"></script>

<script>
date_only="<?php echo $day ?>";
weather_from_db="";
function get_weather_from_db(title){
		$.ajax({
			url: "read_weather.php?titel="+title+"&randval="+Math.random(),
			success: function (result) {
				//console.log(result)
				weather_from_db=result;
			}
		});
}


var chart = c3.generate({
    data: {
        columns: [
            ['Download', <?php echo $data_download ?>],
            ['Upload', <?php echo $data_upload ?>],
            ['Ping', <?php echo $data_ping ?>]
        ]
    },
	tooltip: {
		contents: function (d, defaultTitleFormat, defaultValueFormat, color) {
		var $$ = this, config = $$.config,
              titleFormat = config.tooltip_format_title || defaultTitleFormat,
              nameFormat = config.tooltip_format_name || function (name) { return name; },
              valueFormat = config.tooltip_format_value || defaultValueFormat,
              text, i, title, value, name, bgcolor;
          for (i = 0; i < d.length; i++) {
			  //console.log(d[i].value)
              if (! (d[i] && (d[i].value || d[i].value === 0))) { continue; }

              if (! text) {
                  title = titleFormat ? titleFormat(d[i].x) : d[i].x;
                  text = "<table class='" + $$.CLASS.tooltip + "'>" + (title || title === 0 ? "<tr><th colspan='2'>" + title + "</th></tr>" : "");
              }

              name = nameFormat(d[i].name);
              value = valueFormat(d[i].value, d[i].ratio, d[i].id, d[i].index);
              bgcolor = $$.levelColor ? $$.levelColor(d[i].value) : color(d[i].id);

              text += "<tr class='" + $$.CLASS.tooltipName + "-" + d[i].id + "'>";
              text += "<td class='name'><span style='background-color:" + bgcolor + "'></span>" + name + "</td>";
              text += "<td class='value'>" + value + "</td>";
              text += "</tr>";
          }
		  if(date_only != ""){
			//console.log(title)
		  get_weather_from_db(title);
		  //console.log(weather_from_db)
			return text + "<tr><td>Weather:</td><td>"+weather_from_db+"</td></tr><tr><td colspan='2'><a href='speed_test_img/"+title+"_sky.jpg' target='_blank'><img src='speed_test_img/"+title+"_sky.jpg' width='200' ></a></td></tr><tr><td colspan='2'><a href='speed_test_img/"+title+"_startlink.jpg' target='_blank'><img src='speed_test_img/"+title+"_startlink.jpg' width='200' ></a></td></tr></table>";
		  }else{
			return text;
		  }
  }
},
    zoom: {
        enabled: true
    },
	legend: {
        position: 'right'
    },
    axis: {
        x: {
            type: 'category',
            categories: [<?php echo $data_category ?>]
        }
    }
});

</script>
  </body>
</html>