<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

date_default_timezone_set('Europe/Berlin');

$data=$_GET["titel"];

$servername = "localhost";
$username = "DBuser";
$password = "DBpassword";
$dbname = "starlink";

$d = DateTime::createFromFormat('Y_m_d_H_i_s', $data);
$timestamp = $d->getTimestamp();

//echo "time_stamp: ". $timestamp. " ".$data;

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT datetime, weather FROM internet_speed WHERE datetime='".$timestamp."'";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
  // output data of each row
  while($row = $result->fetch_assoc()) {
	echo $row["weather"];
  }
} else {
  echo "<br>0 results";
}
$conn->close();