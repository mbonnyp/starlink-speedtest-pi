#!/bin/bash


current_timestamp=$( date +%s)
read_last_update=$( cat /home/linux/last_update.txt)

diff=$(( current_timestamp - read_last_update ))

if [[ $diff -ge 1800 ]];then
    echo "ok 30min are over, update weather now"
    /home/linux/weather.sh
    echo $current_timestamp > /home/linux/last_update.txt
else
    echo "30min not over, keep old weather $diff"
fi
