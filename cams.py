from picamera2 import Picamera2, Preview
from time import sleep
import sys
for  eachArg in sys.argv:
	print(eachArg)
current_timestamp=sys.argv[1]


picam0 = Picamera2(0)
picam1 = Picamera2(1)
#picam0.start_preview(Preview.QTGL)
#picam1.start_preview(Preview.QTGL)

preview_config0 = picam0.create_preview_configuration()
capture_config0 = picam0.create_still_configuration()
picam0.configure(preview_config0)

preview_config1 = picam1.create_preview_configuration()
capture_config1 = picam1.create_still_configuration()
picam1.configure(preview_config1)

picam0.start()
picam1.start()
sleep(10)
picam0.switch_mode_and_capture_file(capture_config0, "/var/www/html/speed_test_img/"+current_timestamp+"_startlink.jpg")
picam1.switch_mode_and_capture_file(capture_config1, "/var/www/html/speed_test_img/"+current_timestamp+"_sky.jpg")

picam0.stop()
picam1.stop()
#picam0.stop_preview()
#picam1.stop_preview()
