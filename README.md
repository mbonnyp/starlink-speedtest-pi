This Projetc is done with a "Raspberry Pi 5" and the current "Raspberry Pi OS" i used a old POE HAT where i make a small Cabel to fit the new Connector Position (you also used the normal Usb-C Carger)
Also i connect 2 Pi Cams (v3) to take pics from the Starlink and the Sky (the Pi5 have 2 Slots which you can used as CAM IN or Display Out)
To connect the Raspberry Pi 5 with the "Starlink Dish", i used the "ETHERNET-ADAPTER" and a "POE Injector" for the Power.

Its also possible to used a older PI 4 or 3b but there are only 1 Cam Input avaible and you need a other Hardware if you want used more as 1 Cam.


Attention all Pathes are Hardcoded! This mean in every script i hardcoded all Pathes to my User folder "/home/linux/
You need a Webserver, Databaseserver, PHP+DBconnetction module installed, curl and jq
I used Appache, MariaDB (latest version are fine)

https://randomnerdtutorials.com/raspberry-pi-apache-mysql-php-lamp-server/

After setup Webserver copy all files from "var_www_html" to /var/www/html/{websitename}/ (var/www/html are the default path, modify where evere you setup your webserver)
Install and Setup your Databaseserver and creat a new User with Password a strong Password!, then creat a new DB "internet_speed" and give the user acces to this DB.
At "starlink_db" you will find a sql "internet_speed.sql" file whats creat the needed Table on this DB
Dont forget to Update the User and Password for DB on the 2 php files "index.php" and "read_weather.php" at "/var/www/html/{websitename}/"
Also you need fill the DB User and Password on the "speed_test.sh" line 64

If you like to to see the current Weather Infos you need creat a Account on "openweathermap" and creat a API key whats you place on "weather.sh" on line 2 "API_KEY"
Also you need change the "LAT" and "LON" vars to your location from where you want to get the weather infos
When you dont want to used this infos comment out the line 61 "/home/linux/check_update_weather.sh" on "speed_test.sh"

When you dont want to used the Cams to take Pics after a sucesfully Speedtest run comment out line 68 "python3 /home/linux/cams.py $date_time" on "speed_test.sh"
If you only need 1 Cam (and not two whats i used) modify the "cams.py" as you needed

There are a tone of speedtest CLI version out there and i had troubel find the correct one, so if you also have issued you can used the one from me:
copy the file "speedtest" from "usr_local_bin" to "/usr/local/bin"

Last step create the empty needed files (at your user folder /home/linux/):
hour_weather.txt
last_update.txt
speedtest.log

!!! Pls make sure all files have the correct flags to be Read,Write,Execute!!!
.txt and .log need read+write
.sh , .py and the speedtest need also the execute flag

You can make a Dry test by execute the "speed_test.sh" manuelly, by first run the speedtest CLI will need to accept the license.
Its also possible to config the speedtest CLI there you can google for commands for speedtest CLI or used the help.



After all this you only need install the ChronJob with this command:

crontab -e

Copy this line at the end of the file and save it.

*/5 * * * * /home/linux/speed_test.sh >> /home/linux/speedtest.log

So every 5min the "speed_test.sh" will executed and write to the speedtest.log

A Example with my collected Data from Austria you will find here: https://web.nirusdev.com/starlink/