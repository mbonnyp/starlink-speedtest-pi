#!/bin/bash
ping -c1 8.8.8.8 > /dev/null
current_timestamp=$( date +%s)

startzeit=$current_timestamp
date_time=$(date -d @$startzeit +'%Y_%m_%d_%H_%M_%S')


if [ $? -ne 0 ]; then
        PING=0
        DOWN=0
        UP=0
        STATUS=0
else
        /usr/local/bin/speedtest > /tmp/speedresult.txt
        error=$(cat /tmp/speedresult.txt |grep "ERROR:")
        if [[ "$error" == ""  ]]; then
            while read line; do
            #echo $line;
            if [[ $( echo $line | grep Download:)  ]]; then
                t=$line
                searchstring="Mbps"
                rest=${t#*$searchstring}
                start=$(( ${#t} - ${#rest} - ${#searchstring} ))
                start=$((start-4))
                DOWN=$(echo $line | cut -c 13-$start)
            fi
            if [[ $( echo $line | grep Upload:)  ]]; then
                t=$line
                searchstring="Mbps"
                rest=${t#*$searchstring}
                start=$(( ${#t} - ${#rest} - ${#searchstring} ))
                start=$((start-7))
                UP=$(echo $line | cut -c 11-$start)
            fi
            if [[ $( echo $line | grep Latency:)  ]]; then
                t=$line
                searchstring="ms"
                rest=${t#*$searchstring}
                start=$(( ${#t} - ${#rest} - ${#searchstring} ))
                start=$((start-4))
                PING=$(echo $line | cut -c 10-$start)
            fi
            done < /tmp/speedresult.txt
            STATUS=1
        else
            PING=0
            DOWN=0
            UP=0
            STATUS=0
        fi
fi

echo $PING
echo $DOWN
echo $UP
echo $STATUS
echo $current_timestamp
echo $date_time

/home/linux/check_update_weather.sh
curren_weather=$( cat /home/linux/hour_weather.txt )

echo "INSERT INTO starlink.internet_speed (weather,datetime,ping,down,up,status) VALUES ('$curren_weather',$current_timestamp,$PING,$DOWN,$UP,$STATUS);" | mysql --user=DBuser --password=DBpassword

rm -f /tmp/speedresult.txt

python3 /home/linux/cams.py $date_time
