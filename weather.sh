#!/bin/bash
API_KEY=xxxxxxxxxxxxx
LAT=00.00
LON=00.00
api_weather=$( curl -s "https://api.openweathermap.org/data/3.0/onecall?lat=$LAT&lon=$LON&exclude=minutely,hourly,daily,alerts&appid=$API_KEY&units=metric" )

api_weather_now=$( echo $api_weather | jq '.current.weather' )
api_weather_temp=$( echo $api_weather | jq '.current.temp' )



api_weather_now_main=$( echo $api_weather_now | jq '.[].main' | sed 's/"//g')
api_weather_now_description=$(echo $api_weather_now | jq '.[].description' | sed 's/"//g')

echo "$api_weather_temp° $api_weather_now_description" > /home/linux/hour_weather.txt
#echo "main: $api_weather_now_main"
#echo "description: $api_weather_now_description" 
